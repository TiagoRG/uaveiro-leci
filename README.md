# UAveiro - LECI | [Wiki](https://github.com/TiagoRG/uaveiro-leci/wiki)
### Todo o trabalho feito ao longo do curso de Licenciatura em Engenharia de Computatores e Informática

Pode usar todo o material/código/etc. disponibilizado mas é expressamente proibido fazer cópia direta do que aqui está presente pois poderá resultar em consequência indesejadas.

---

# Índice
## [1º Ano](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre)
- ### [1º Semestre](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre)
  - [Fundamentos de Programação](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/fp)
  - [Introdução à Engenharia Informática](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/iei)
  - [Introdução aos Sistemas Digitais](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/isd)
  - [&Aacute;lgebra Linear e Geometria Anal&iacute;tica](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/alga)
  - [C&aacute;lculo - 1](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/c1)

- ### [2º Semestre](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre)
  - [Programação Orientada a Objetos](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre/poo)
  - [Laboratórios de Informática](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre/labi)
  - [Laboratório de Sistemas Digitais](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre/lsd)
  - [Matem&aacute;tica Discreta](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre/md)
  - [C&aacute;lculo - 2](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/2semestre/c2)

---
*Pode conter erros, caso encontre algum, crie um* [*ticket*](https://github.com/TiagoRG/uaveiro-leci/issues/new)
