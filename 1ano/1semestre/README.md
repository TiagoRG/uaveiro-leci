# 1º Semestre
## Todo o material das cadeiras do 1º semestre do 1º ano:

- [Fundamentos de Programação](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/fp)
- [Introdução à Engenharia Informática](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/iei)
- [Introdução aos Sistemas Digitais](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/isd)
- [&Aacute;lgebra Linear e Geometria Anal&iacute;tica](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/alga)
- [C&aacute;lculo - 1](https://github.com/TiagoRG/uaveiro-leci/tree/master/1ano/1semestre/c1)

---
*Pode conter erros, caso encontre algum, crie um* [*ticket*](https://github.com/TiagoRG/uaveiro-leci/issues/new)
